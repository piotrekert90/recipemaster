package com.example.piotr.recipemaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void viewExtendedMenu(View v) {
        Intent intent = new Intent(this, ExtendedMenuActivity.class);
        startActivity(intent);
    }
}
