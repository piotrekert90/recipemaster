package com.example.piotr.recipemaster;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by piotr on 26.08.15.
 */
public class ExtendedMenuActivity extends MainActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void returnToMainActivity(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void goToGetRecipeActivity(View v) throws InterruptedException {
        Intent intent = new Intent(this, GetRecipeActivity.class);
        startActivity(intent);
    }
}