package com.example.piotr.recipemaster;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

public class GetRecipeActivity extends MainActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        startNewRecipeActivity();
    }

    protected void startNewRecipeActivity() {
        JSONParser parser = new JSONParser();
        parser.execute();
        getNewRecipe(parser.jsonString);
    }

    public void getNewRecipe(String JSONString) {
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        TextView textViewDescription = (TextView) findViewById(R.id.textViewDescription);
        TextView textViewIngredients = (TextView) findViewById(R.id.ingredients);
        TextView textViewIngredientsList = (TextView) findViewById(R.id.ingredients);
        TextView textViewPreparing = (TextView) findViewById(R.id.preparing);
        TextView textViewPreparingList = (TextView) findViewById(R.id.preparingList);
        TextView textViewGallery = (TextView) findViewById(R.id.imgGalleryText);
        ImageView imageViewGallery1 = (ImageView) findViewById(R.id.galleryImg1);
        ImageView imageViewGallery2 = (ImageView) findViewById(R.id.galleryImg2);
        ImageView imageViewGallery3 = (ImageView) findViewById(R.id.galleryImg3);


        try {
            JSONObject entireJSON = new JSONObject(JSONString);
            setRecipeTitle(textViewTitle, entireJSON);
            setRecipeDescription(textViewDescription, entireJSON);
            setRecipeIngredients(textViewIngredients, textViewIngredientsList, entireJSON);
            setRecipePreparing(textViewPreparing, textViewPreparingList, entireJSON);
            setRecipeGallery(textViewGallery, imageViewGallery1, imageViewGallery2, imageViewGallery3, entireJSON);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setRecipeGallery(TextView textViewGallery, ImageView imageViewGallery1, ImageView imageViewGallery2, ImageView imageViewGallery3, JSONObject entireJSON) throws JSONException {
        textViewGallery.setText("Gallery:");

        JSONArray gallery = entireJSON.getJSONArray("imgs");

        String imageUrl1 = gallery.getString(0);
        String imageUrl2 = gallery.getString(1);
        String imageUrl3 = gallery.getString(2);

        Picasso.with(getApplicationContext()).load(imageUrl1).into(imageViewGallery1);
        Picasso.with(getApplicationContext()).load(imageUrl2).into(imageViewGallery2);
        Picasso.with(getApplicationContext()).load(imageUrl3).into(imageViewGallery3);
    }

    private void setRecipePreparing(TextView textViewPreparing, TextView textViewPreparingList, JSONObject entireJSON) throws JSONException {
        textViewPreparing.setText("Preparing:");
        JSONArray preparing = entireJSON.getJSONArray("preparing");
        textViewPreparingList.setText(preparing.toString(10));
    }

    private void setRecipeIngredients(TextView textViewIngredients, TextView textViewIngredientsList, JSONObject entireJSON) throws JSONException {
        textViewIngredients.setText("Ingredients:");
        JSONArray ingredients = entireJSON.getJSONArray("ingredients");
        textViewIngredientsList.setText(ingredients.toString(3));
    }

    private void setRecipeDescription(TextView textViewDescription, JSONObject entireJSON) throws JSONException {
        String description = entireJSON.getString("description");
        textViewDescription.setText(description);
    }

    private void setRecipeTitle(TextView textViewTitle, JSONObject entireJSON) throws JSONException {
        String title = entireJSON.getString("title");
        textViewTitle.setText(title);
    }

    public void saveImageToExternalMemory(View view) {
        if (view != null) {

            System.out.println("view is not null.....");
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache();
            Bitmap bm = view.getDrawingCache();

            try {
                if (bm != null) {

                    String dir = Environment.getExternalStorageDirectory().toString();
                    System.out.println("bm is not null.....");
                    OutputStream fos = null;
                    File file = new File(dir, UUID.randomUUID().toString() + ".JPEG");
                    fos = new FileOutputStream(file);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
                    bm.compress(Bitmap.CompressFormat.JPEG, 50, bos);
                    bos.flush();
                    bos.close();
                    Toast.makeText(getApplicationContext(), "Image saved with success",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                System.out.println("Error=" + e);
                e.printStackTrace();
            }
        }
    }
    //TODO: Add background process (AsyncTask or Service)
    //TODO: Tidy up layout, especially string
}
