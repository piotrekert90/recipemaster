package com.example.piotr.recipemaster;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.example.piotr.recipemaster.TestMethods.checkIfViewIsDisplayed;

/**
 * Created by piotr on 01.09.15.
 */
@RunWith(AndroidJUnit4.class)
public class ExtendedMenuActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkIfExtendedMenuActivityBackgroundIsDisplayed() {
        goToExpandedMenuView();
        checkIfViewIsDisplayed(R.id.main_background);
    }

    @Test
    public void checkIfAllImageButtonsHintsAreCompletelyDisplayed() {
        goToExpandedMenuView();
        checkIfViewIsDisplayed(R.id.get_recipe_hint);
        checkIfViewIsDisplayed(R.id.login_facebook_hint);
    }

    @Test
    public void checkIfAllImageButtonsAreCompletelyDisplayed() {
        goToExpandedMenuView();
        checkIfViewIsDisplayed(R.id.get_recipe);
        checkIfViewIsDisplayed(R.id.login_facebook);
        checkIfViewIsDisplayed(R.id.return_button);
    }

    private void goToExpandedMenuView() {
        onView(withId(R.id.expandedMenuButton)).perform(click());
    }


    @Test
    public void checkGoToGetRecipeActivity() {
        goToExpandedMenuView();
        checkIfViewIsDisplayed(R.id.get_recipe);
        onView(withId(R.id.get_recipe)).perform(click());
        onView(withId(R.drawable.return_btn)).check(doesNotExist());

    }

    @Test
    public void checkFacebookLoginActivity() {
        goToExpandedMenuView();
        checkIfViewIsDisplayed(R.id.login_facebook);

    }

    @Test
    public void checkReturnButton() {
        goToExpandedMenuView();
        checkIfViewIsDisplayed(R.id.return_button);
        onView(withId(R.id.return_button)).perform(click());
        checkIfViewIsDisplayed(R.id.expandedMenuButton);
    }
}
