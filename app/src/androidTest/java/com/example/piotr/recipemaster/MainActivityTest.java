package com.example.piotr.recipemaster;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.example.piotr.recipemaster.TestMethods.checkIfViewIsDisplayed;

/**
 * Created by piotr on 01.09.15.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkIfAppNameisDisplayed() {
        onView(withText(R.string.app_name)).check(matches(isDisplayed()));
    }

    @Test
    public void checkIfMainActivityViewIsCompletelyDisplayed() {
        checkIfViewIsDisplayed(R.id.main_background);
        checkIfViewIsDisplayed(R.drawable.menu_btn);
    }

    @Test
    public void checkGoToExtendedMenuActivity() {
        checkIfViewIsDisplayed(R.id.expandedMenuButton);
        onView(withId(R.id.expandedMenuButton)).perform(click());
        checkIfViewIsDisplayed(R.drawable.login_fb_btn);
    }
}
